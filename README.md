# GoodFood V2

**CESI MSI 2020-2022, Lézapéricubes**

Serveur développé en Python à l'aide du framework [Django Oscar](https://github.com/django-oscar/django-oscar) en tant qu'application web.



## Lancer l'application

Si vous souhaitez lancer l'application, nous vous recommandons d'utiliser l'IDE Pycharm.

Après avoir obtenu le code du dépot, vous pouvez l'ouvrir à l'aide de Pycharm.

Une fois le projet ouvert dans l'IDE, il faut [créer un environnement virtuel](https://www.jetbrains.com/help/pycharm/creating-virtual-environment.html) en suivant le tutoriel.

La création d'un environnement virtuel terminée, il vous faut ouvrir la console présente dans Pycharm et y taper la commande: `pip install -r "requirements.txt"` cela permettra d'installer tous les packets nécessaires au lancement du projet.

Il faut ensuite ajouter un fichier ".env" dans le dossier ou se trouve le fichier 'settings.py'

```cfg
API_KEY=[Google api key]
DB_HOST=[DB Host Adress]
DB_USER=[DB User]
DB_NAME=[DB Name]
DB_PASS=[DB Password]
DEBUG=True
ALLOWED_HOSTS=
SECRET_KEY=
EMAIL_HOST='localhost'
EMAIL_PORT=1025
EMAIL_HOST_USER=''
EMAIL_HOST_PASSWORD=''
EMAIL_USE_TLS=False
DEFAULT_FROM_EMAIL='mail@mail.fr'
```

Ce fichier permet de configurer votre environnement sans le partager dans vos fork.

Une fois ces commandes effectuées, il vous faudra effectuer les migrations de bases de données comme présentées [ici](https://docs.djangoproject.com/en/3.2/topics/migrations/)

La configuration est maintenant terminée, vous pouvez dès à présent lancer le projet. 


## Modules utilisés

Voici la liste des dépendances principales présentes dans le projet.

- **Django Oscar** - Framework E-Commerce basé sur le Framework Python Django.
- **Django PWA** - Module Django permettant la génération d'une Progressive Web App (PWA).
- **Django Oscar API** - Module Django permettant la génération d'une API.

## Architecture Logiciel

Cette partie vous permettra de savoir ou trouver les différents dossiers du projet et leur utilité.

### **/template/**

Dossier contenant tous les templates pour les différentes pages de l'application. Si vous souhaitez modifier l'apparence du site, il faut modifier les fichiers dans ce dossier.

### **/static/**

Dossier contenant tous les fichiers statics: les fichiers CSS, JS, et autre. Permettant l'affichage mis en page du projet.

### **Autres dossiers**

Les autres dossiers ayant des noms de modules permettent la modification de la logique du projet.


## Collaborateurs

- Fache Bruno
- Matter Killian
- Arlen Rémi
- Fornazaric Florian