import oscar.apps.dashboard.apps as apps


class DashboardConfig(apps.DashboardConfig):
    name = 'good_food.dashboard'
