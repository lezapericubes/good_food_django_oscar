from oscar.apps.dashboard.catalogue import forms as base_forms

from good_food.stores.models import StoreStock


class StockRecordForm(base_forms.StockRecordForm):
    def __init__(self, product_class, user, *args, **kwargs):
        super().__init__(product_class, user, *args, **kwargs)
        self.fields["partner_sku"].initial = product_class.sku

    class Meta(base_forms.StockRecordForm.Meta):
        model = StoreStock
        fields = [
            "partners",
            "partner_sku",
            "price_currency",
            "price",
            "num_in_stock",
            "low_stock_threshold",
        ]


from oscar.apps.dashboard.catalogue import *
