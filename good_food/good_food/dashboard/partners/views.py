from oscar.apps.dashboard.partners import views as base_views
from django.utils.translation import gettext_lazy as _
from django.contrib import messages


class PartnerManageView(base_views.PartnerManageView):

    def form_valid(self, form):
        messages.success(
            self.request, _("Partner '%s' was updated successfully.") % self.partner.name)
        return super(base_views.PartnerManageView, self).form_valid(form)
