from oscar.apps.dashboard.partners import forms as base_forms
from django import forms
from django.utils.translation import pgettext_lazy


class PartnerAddressForm(base_forms.PartnerAddressForm):
    name = forms.CharField(
        required=False, max_length=128,
        label=pgettext_lazy("Partner's name", "Name"))

    is_provider = forms.BooleanField(
        required=True, label=pgettext_lazy("Is partner provider ?", "Is Provider"))

    class Meta(base_forms.PartnerAddressForm.Meta):
        fields = ('name', 'is_provider', 'line1', 'line2', 'line3',
                  'postcode', 'country')

from oscar.apps.dashboard.partners.forms import *