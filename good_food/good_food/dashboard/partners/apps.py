import oscar.apps.dashboard.partners.apps as apps
from oscar.core.loading import get_class

class PartnersDashboardConfig(apps.PartnersDashboardConfig):
    name = 'good_food.dashboard.partners'

    def ready(self):
        super().ready()
        self.manage_view = get_class('dashboard.partners.views', 'PartnerManageView')
