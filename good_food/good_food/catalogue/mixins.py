class StorePartnerStockRecordFilterMixin:
    def filter_queryset(self, queryset):
        """
        Restrict the queryset to products where stock record is associated to a partner associated to the selected
        store.
        """
        store = self.request.basket.store

        return queryset.filter(product__stockrecord__partner__users__pk=store.partner.pk)
