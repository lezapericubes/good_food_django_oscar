from oscar.core.loading import get_model
from oscar.core.loading import get_class
from oscar.apps.catalogue.views import CatalogueView as ParentCatalogueView, \
    ProductCategoryView as ParentProductCategoryView

Basket = get_model("basket", "Basket")
StorePartnerStockRecordFilterMixin = get_class(
    "catalogue.mixins", "StorePartnerStockRecordFilterMixin"
)


class CatalogueView(StorePartnerStockRecordFilterMixin, ParentCatalogueView):
    pass


class ProductCategoryView(StorePartnerStockRecordFilterMixin, ParentProductCategoryView):
    pass


from oscar.apps.catalogue.views import *  # noqa isort:skip
