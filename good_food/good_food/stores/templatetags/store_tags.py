from django import template
from oscar.apps.partner.strategy import Selector
from django.conf import settings
# from oscar.apps.basket.models import Basket
from oscar.core.loading import get_model

register = template.Library()
Partner = get_model("partner", "partner")
Store = get_model("stores", "Store")
Basket = get_model("basket", "Basket")
PartnerStockRecord = get_model("partner", "StockRecord")


@register.simple_tag(name="store_list")
def store_list():
    return Store.objects.all()

@register.simple_tag(name="partner_list")
def partner_list():
    return Partner.objects.all()


@register.simple_tag(name="get_store", takes_context=True)
def get_store(context):
    request = context['request']
    store = request.basket.store
    return store


@register.simple_tag(name="get_user", takes_context=True)
def get_user(context):
    request = context['request']
    store = request.user
    return store


def get_context(max_depth=4):
    import inspect
    stack = inspect.stack()[2:max_depth]
    context = {}
    for frame_info in stack:
        frame = frame_info[0]
        arg_info = inspect.getargvalues(frame)
        if 'context' in arg_info.locals:
            context = arg_info.locals['context']
            break
    return context


def is_valid(partner, store, user):
    # if the user is a member of a partner staff, show the list of products from the providers
    if (user.is_staff or user in partner.users.all()) and partner.is_provider:
        return True
        
    # if the partner of the stockrecord of the product is associated with the selected store, return True
    if partner.pk == store.partner.pk:
        return True

    return False


# @register.simple_tag(takes_context=True, name="partner_validation")
@register.filter(name="store_validation")
def store_validation(product):
    context = get_context()
    store = get_store(context)
    user = get_user(context)

    products = []
    if (not product.is_child) and product.children:
        for child in product.children.public():
            # Use tuples of (child product, stockrecord)
            products.append(child)

    if not products:
        products.append(product)

    records = PartnerStockRecord.objects.all()
    # check if the product is valid and should be shown
    invalid = True
    for record in records:
        if record.product in products and is_valid(record.partner, store, user):
            invalid = False
            break

    return invalid
