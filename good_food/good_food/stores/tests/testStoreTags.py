from django.test import SimpleTestCase

from good_food.stores.templatetags.store_tags import store_list
from oscar.core.loading import get_model
Store = get_model("stores", "Store")

class TrivialTest(SimpleTestCase):
    def testAddition(self):
        """
        Tests that 1 + 1 always equals 2.
        """
        self.assertEqual(1 + 1, 2)
