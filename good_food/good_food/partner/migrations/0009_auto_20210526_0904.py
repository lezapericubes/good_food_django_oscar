# Generated by Django 3.1.5 on 2021-05-26 09:04

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('partner', '0008_auto_20210526_0852'),
    ]

    operations = [
        migrations.AlterField(
            model_name='openinghour',
            name='valid_from',
            field=models.DateField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='openinghour',
            name='valid_throught',
            field=models.DateField(blank=True, null=True),
        ),
    ]
