# Generated by Django 3.1.5 on 2021-05-26 08:25

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('partner', '0006_auto_20200724_0909'),
    ]

    operations = [
        migrations.CreateModel(
            name='OpeningHour',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('closes', models.TimeField()),
                ('day_of_week', models.CharField(default=None, max_length=200)),
                ('opens', models.TimeField()),
                ('valid_from', models.DateField()),
                ('valid_throught', models.DateField()),
            ],
        ),
    ]
