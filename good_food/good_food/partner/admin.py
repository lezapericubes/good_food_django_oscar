from django.contrib import admin
from oscar.core.loading import get_model

StockRecord = get_model('partner', 'StockRecord')
PartnerAddress = get_model('partner', 'PartnerAddress')
OpeningHour = get_model('partner', 'OpeningHour')
Partner = get_model('partner', 'Partner')


class OpeningHoursInLineAdmin(admin.TabularInline):
    model = OpeningHour
    extra = 0


class PartnerAddressInlineAdmin(admin.TabularInline):
    model = PartnerAddress
    extra = 0


class PartnerAdmin(admin.ModelAdmin):
    inlines = (OpeningHoursInLineAdmin, PartnerAddressInlineAdmin,)


class StockRecordAdmin(admin.ModelAdmin):
    list_display = ('product', 'partner', 'partner_sku', 'price', 'num_in_stock')
    list_filter = ('partner',)


admin.site.register(Partner, PartnerAdmin)
admin.site.register(StockRecord, StockRecordAdmin)
