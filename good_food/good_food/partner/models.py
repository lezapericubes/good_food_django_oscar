from django.db import models

from oscar.core.loading import is_model_registered
from django.utils.translation import gettext_lazy as _
from oscar.apps.partner.abstract_models import AbstractPartner

if not is_model_registered("partner", "OpeningHour"):

    class OpeningHour(models.Model):
        MONDAY = "mon"
        TUESDAY = "tue"
        WEDNESDAY = "wed"
        THURSDAY = "thu"
        FRIDAY = "fri"
        SATURDAY = "sat"
        SUNDAY = "sun"
        PUBLIC_HOLIDAYS = "pub"
        DAY_OF_WEEK = (
            (MONDAY, _("Monday")),
            (TUESDAY, _("Tuesday")),
            (WEDNESDAY, _("Wednesday")),
            (THURSDAY, _("Thursday")),
            (FRIDAY, _("Friday")),
            (SATURDAY, _("Saturday")),
            (SUNDAY, _("Sunday")),
            (PUBLIC_HOLIDAYS, _("Public Holidays")),
        )
        partner = models.ForeignKey(
            "partner.Partner",
            on_delete=models.CASCADE,
            verbose_name=_("Partner"),
            related_name="openinghour",
        )

        day_of_week = models.CharField(
            max_length=3, default=None, blank=False, null=False, choices=DAY_OF_WEEK
        )
        opens = models.TimeField(editable=True, blank=False)
        closes = models.TimeField(editable=True, blank=False)
        valid_from = models.DateField(editable=True, blank=True, null=True)
        valid_throught = models.DateField(editable=True, blank=True, null=True)

        def __str__(self):
            return self.day_of_week


if not is_model_registered("partner", "Partner"):

    class Partner(AbstractPartner):
        is_provider = models.BooleanField(default=False, blank=False, null=False)
        description = models.TextField(default=None, blank=True, null=True)


from oscar.apps.partner.models import *
