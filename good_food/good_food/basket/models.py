from oscar.apps.basket.abstract_models import (
    AbstractBasket)

from oscar.core.loading import get_model
from django.db import models
from django.utils.translation import gettext_lazy as _

Store = get_model("stores", "Store")


class Basket(AbstractBasket):
    store = models.ForeignKey(
        Store,
        verbose_name=_("Store"),
        related_name="store",
        null=True,
        on_delete=models.SET_NULL,
    )


from oscar.apps.basket.models import *  # noqa isort:skip
