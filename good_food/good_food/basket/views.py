def SelectStoreView(request, store_pk):
    basket = request.basket
    # if store change, reset the basket
    if basket.store_id != store_pk:
        basket.store_id = store_pk
        basket.flush()
        basket.save()
    return redirect('catalogue:index')

from oscar.apps.basket.views import *  # noqa isort:skip