import oscar.apps.basket.apps as apps
from django.urls import path
from oscar.core.loading import get_class


class BasketConfig(apps.BasketConfig):
    name = 'good_food.basket'

    def ready(self):
        self.select_store_view = get_class('basket.views', 'SelectStoreView')
        super(BasketConfig, self).ready()

    def get_urls(self):
        additional_urls = [
            path('<int:store_pk>/select-store/', self.select_store_view, name='select-store'),
        ]
        urls = self.post_process_urls(additional_urls)
        urls += super(BasketConfig, self).get_urls()

        return urls
